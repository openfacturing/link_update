package models

import java.io._
import java.nio.file.Files
import java.security.MessageDigest
import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

/**
  * Created by unomic on 2018. 10. 5..
  */


class Base64_Func {


  def File_to_base64(file_path : String): String = {

    var file = new File(file_path)

    var file_array = Files.readAllBytes(file.toPath());
    var encoded = Base64.getEncoder().encode(file_array);
    var base64 = new String(encoded)

    return base64
  }

  def base64_to_file(base64: String, output_path : String): Unit ={
    var byte_decode = Base64.getDecoder().decode(base64);
    var file_output = new FileOutputStream(output_path);
    file_output.write(byte_decode);
    file_output.close();
  }


}


object Base64_Func {

  var b64 = new Base64_Func()

}