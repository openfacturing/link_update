package controllers

import java.io.File
import java.net.URL

import play.api._
import play.api.mvc._
import models.AES_Func.afs
import models.Base64_Func.b64
import models.Func.func
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.params.HttpConnectionParams
import org.apache.http.util.EntityUtils

import sys.process._
import java.net.URL

import org.apache.commons.io.FileUtils
import org.json.JSONObject

import scala.io.Source

class Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def update = Action {
    Ok(views.html.update())
  }



  def link_version_info = Action {

    var result = new org.json.JSONObject()

    var agent_version_file_path = "/root/nc_agent/version_info.aes"
    var raw_version_file_path = "/root/ncrawversion.aes"

    var agent_version_file = new File(agent_version_file_path)
    var raw_version_file = new File(raw_version_file_path)

    var agent_version = ""
    var raw_version = ""

    if(agent_version_file.exists()) {

      var version = ""

      try {

        version = afs.Decoding_Content(agent_version_file_path)

        var num = version.toDouble
        agent_version = version

      } catch {
        case e : Exception => {
          e.printStackTrace();
          func.Log_error(e);
          agent_version = "잘못된 version 정보입니다 ( " + version + ")"
        };
      }

    } else {
      agent_version = "version 정보가 없습니다"
    }


    if(raw_version_file.exists()) {

      var version = ""

      try {

        version = afs.Decoding_Content(raw_version_file_path)

        var num = version.toDouble
        raw_version = version

      } catch {
        case e : Exception => {
          e.printStackTrace();
          func.Log_error(e);
          raw_version = "잘못된 version 정보입니다 ( " + version + ")"
        };
      }

    } else {
      raw_version = "version 정보가 없습니다"
    }

    result.put("agent_version",agent_version)
    result.put("raw_version",raw_version)

    Ok(result.toString)
  }

//  def server_link_version_info = Action {
//
//    var result = ""
//
//    var server_url = "http://106.240.234.116:8001/server_link_version"
//
//    var http_client = new DefaultHttpClient();
//
//    HttpConnectionParams.setConnectionTimeout(http_client.getParams, 5 * 1000);
//    HttpConnectionParams.setSoTimeout(http_client.getParams, 5 * 1000);
//
//    try {
//      val request = new HttpGet(server_url)
//
//      val response = http_client.execute(request)
//      val response_code = response.getStatusLine.getStatusCode
//
//      val entity = response.getEntity
//      result = EntityUtils.toString(entity, "UTF-8")
//      response.close()
//
//      if(result.contains("\ufeff")){
//        result = result.replaceAll("\ufeff", "")
//      }
//
//      println("result : " + result)
//
//    } catch {
//
//      // 주로 Http 통신 에러
//      case e: Exception => {
//        e.printStackTrace()
//        func.Log_error(e);
//        result =
//          """
//            |{
//            | "server_agent_version" : "server connect fail",
//            | "server_raw_version" : "server connect fail"
//            |}
//          """.stripMargin
//      }
//    }
//
//    Ok(result)
//  }

  def server_link_version_info = Action {

    var result = ""

    try {
      // config file read
      var config_file = new File("/root/nas_info.json");

      // nas_info.json  Example
      """
        |{
        |   "agent_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/nc_agent.jar",
        |   "agent_version_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/version_info.txt",
        |
        |   "raw_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/OpcUaAdapter.jar-jar-with-dependencies.jar",
        |   "raw_version_url" :"http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/ncrawversion.txt"
        |}
      """.stripMargin

      var config_file_content = FileUtils.readFileToString(config_file, "utf-8");
      var config_file_json = new JSONObject(config_file_content);

      var server_agent_version_url = config_file_json.getString("agent_version_url")
      var server_raw_version_url = config_file_json.getString("raw_version_url")

      val agent_url = new URL(server_agent_version_url)
      val raw_url = new URL(server_raw_version_url)

      agent_url #> new File("server_agent_version.txt") !!

      raw_url #> new File("server_raw_version.txt") !!

      var agent_file = new File("server_agent_version.txt")
      var raw_file = new File("server_raw_version.txt")

      var server_agent_version = ""
      var server_raw_version = ""

      if(agent_file.exists() && raw_file.exists()) {

        server_agent_version = Source.fromFile("server_agent_version.txt").getLines().next()
        server_raw_version = Source.fromFile("server_raw_version.txt").getLines().next()

        agent_file.delete()
        raw_file.delete()
      }

      result =
        s"""
          |{
          | "server_agent_version" : "${server_agent_version}",
          | "server_raw_version" : "${server_raw_version}"
          |}
        """.stripMargin

    } catch {

      // 주로 Http 통신 에러
      case e: Exception => {
        e.printStackTrace()
        func.Log_error(e);
        result =
          """
            |{
            | "server_agent_version" : "server connect fail",
            | "server_raw_version" : "server connect fail"
            |}
          """.stripMargin
      }
    }

    Ok(result)
  }


  def agent_update = Action {

    println("Agent 서버 접속 시도")

    // config file read
    var config_file = new File("/root/nas_info.json");

    // nas_info.json  Example
    """
      |{
      |   "agent_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/nc_agent.jar",
      |   "agent_version_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/version_info.txt",
      |
      |   "raw_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/OpcUaAdapter.jar-jar-with-dependencies.jar",
      |   "raw_version_url" :"http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/ncrawversion.txt"
      |}
    """.stripMargin

    var config_file_content = FileUtils.readFileToString(config_file, "utf-8");
    var config_file_json = new JSONObject(config_file_content);

    var agent_nas_url = config_file_json.getString("agent_file_url")

    fileDownloader(agent_nas_url,"/root/key/nc_agent.jar")

    println("Agent 접속 성공")

    Ok("SU")

  }

  def raw_update = Action {

    println("raw 서버 접속 시도")

    // config file read
    var config_file = new File("/root/nas_info.json");

    // nas_info.json  Example
    """
      |{
      |   "agent_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/nc_agent.jar",
      |   "agent_version_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/agent/version_info.txt",
      |
      |   "raw_file_url" : "http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/OpcUaAdapter.jar-jar-with-dependencies.jar",
      |   "raw_version_url" :"http://unomic.ipdisk.co.kr:80/publist/HDD1/Data/kisa/raw/ncrawversion.txt"
      |}
    """.stripMargin

    var config_file_content = FileUtils.readFileToString(config_file, "utf-8");
    var config_file_json = new JSONObject(config_file_content);

    var raw_nas_url = config_file_json.getString("raw_file_url")

    fileDownloader(raw_nas_url,"/root/key/OpcUaAdapter.jar-jar-with-dependencies.jar")

    println("raw 서버 접속 성공")

    Ok("SU")
  }


  def script_run(process: String) = Action {

    util_1_script()
    util_2_script(process)
    util_3_script(process)
    test_script(process)

    Ok("")
  }


  def util_1_script() = {

    println("util.sh Start")

    var pri_key_file = new File("/root/key/pri.key")
    var pub_key_file = new File("/root/key/pub.key")

    if(pri_key_file.exists() && pub_key_file.exists()) {
      // 공개키, 개인키가 모두 있는 경우
      println("Key Exist")

    } else{
      // 공개키, 개인키중 하나라도 없는 경우 util.sh 실행

      var command = "/root/key/util.sh"

      var run = func.runSudoCommand(command.split(" "),"root")

    }

    println("util.sh Success")

  }

  def util_2_script(process: String) = {

    println("util2_" + process + ".sh Start")

    var command = "/root/key/util2_" + process + ".sh"

    var run = func.runSudoCommand(command.split(" "),"root")

    println("util2_" + process + ".sh Success")

  }

  def util_3_script(process: String) = {

    println("util3_" + process + ".sh Start")

    var command = "/root/key/util3_" + process + ".sh"

    var run = func.runSudoCommand(command.split(" "),"root")

    println("util3_" + process + ".sh Success")

  }

  def test_script(process: String) : String = {

    println("test_" + process + ".sh Start")

    var result = ""

    var command = "/root/key/test_" + process + ".sh"

    var run = func.runSudoCommand(command.split(" "),"root")

    var last_str5 = run.substring(run.length-5, run.length)
    println("last string : " + last_str5)

    if(last_str5.contains("TRUE")){
      result = "true"
    }

    if(last_str5.contains("FALSE")){
      result = "false"
    }

    println("test_" + process + ".sh Success")

    println(process + " mv start")

    if(result.equals("true")) {

      if(process.equals("agent")){

        var rm_command = "sudo rm -rf /root/nc_agent/nc_agent.jar"
        func.runSudoCommand(rm_command.split(" "),"root")

        var mv_command = "sudo mv /root/key/nc_agent.jar /root/nc_agent/"
        func.runSudoCommand(mv_command.split(" "),"root")

        println(process + " mv Success")

      }

      if(process.equals("raw")){

        var rm_command = "sudo rm -rf /root/OpcUaAdapter.jar-jar-with-dependencies.jar"
        func.runSudoCommand(rm_command.split(" "),"root")

        var mv_command = "sudo mv /root/key/OpcUaAdapter.jar-jar-with-dependencies.jar /root/"
        func.runSudoCommand(mv_command.split(" "),"root")

        println(process + " mv Success")
      }

    } else {
      throw new Exception("Exception!!");
    }

    return result

  }


  def link_reboot = Action {

    println("reboot.sh Start")

    var command = "/root/key/reboot.sh"

    func.runSudoCommand(command.split(" "),"root")

    Ok("SU")

  }

  def fileDownloader(url: String, filename: String) = {
    new URL(url) #> new File(filename) !!
  }


}