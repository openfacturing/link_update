/**
 * Created by jeongwan on 2017. 10. 11..
 */


var targetWidth = 3840;
var targetHeight = 2160;

var originWidth = window.innerWidth;
var originHeight = window.innerHeight;

var contentWidth = originWidth;
var contentHeight = targetHeight/(targetWidth/originWidth);

function getElSize(n){
    return contentWidth/(targetWidth/n);
};

var agent_update_button_result = true;
var raw_update_button_result = true;


$(function(){

    setEl();

    link_version_insert();

    server_link_version_insert();


});

function setEl(){

    $("#title_font").css({
        "font-size" : getElSize(60) + "px"
    });

    $("#agent_update_button").css({
        // "height": getElSize(70) + "px",
        "width": getElSize(550) + "px",
        "font-size" : getElSize(45) + "px",
        "text-align" : "center",
        "vertical-align" : "middle",
        // "padding" : getElSize(15) + "px"
    });

    $("#raw_update_button").css({
        // "height": getElSize(70) + "px",
        "width": getElSize(550) + "px",
        "font-size" : getElSize(45) + "px",
        "text-align" : "center",
        "vertical-align" : "middle",
        // "padding" : getElSize(15) + "px"
    });

    $(".update_font").css({
        "font-size" : getElSize(50) + "px"
    });


};



function link_version_insert() {

    var url = "link_version_info";

    $.ajax({
        url: url,
        dataType: "json",
        type: "get",
        success: function (Data) {

            console.log("Data : " + JSON.stringify(Data))

            var agent_version = Data.agent_version
            var raw_version = Data.raw_version

            console.log("agent_version : " + agent_version)

            document.getElementById("agent_version_font").innerText = agent_version;
            document.getElementById("raw_version_font").innerText = raw_version;

            if(!isNumber(agent_version)){
                agent_update_button_result = false
            }

            if(!isNumber(raw_version)){
                raw_update_button_result = false
            }

        }, error : function (Data) {
            agent_update_button_result = false
            raw_update_button_result = false
            alert("Link 버전 정보를 가져오는데 실패하였습니다");
        }
    });

}



function server_link_version_insert() {

    var url = "server_link_version_info";

    $.ajax({
        url: url,
        dataType: "json",
        type: "get",
        success: function (Data) {

            var server_agent_version = Data.server_agent_version
            var server_raw_version = Data.server_raw_version

            document.getElementById("server_agent_version_font").innerText = server_agent_version;
            document.getElementById("server_raw_version_font").innerText = server_raw_version;

            if(!isNumber(server_agent_version)){
                agent_update_button_result = false
            }

            if(!isNumber(server_raw_version)){
                raw_update_button_result = false
            }

        }, error : function (Data) {
            agent_update_button_result = false
            raw_update_button_result = false
            alert("서버 접속에 실패하였습니다");
        }
    });


}



function Agent_Update() {

    if(!agent_update_button_result) {
        alert("Agent 버전 정보 및 서버 연결상태를 확인해주세요")
        return
    }

    var target = document.body
    var spinner = new Spinner().spin(target);



    var url = "agent_update";

    $.ajax({
        url: url,
        dataType: "text",
        type: "get",
        success: function (Data) {

            $.ajax({
                url: "script_run",
                data : "process=agent",
                dataType: "text",
                type: "get",
                success: function (Data) {

                    alert("Agent 업데이트 완료")

                    alert("Link 를 재부팅합니다")

                    spinner.stop()

                    $.ajax({
                        url : "/link_reboot",
                        dataType : "text",
                        type: "get",
                        success : function (data) {

                        },error : function (data) {

                        }
                    })

                }, error : function (Data) {

                    alert("Agent 업데이트 실패");

                }, complete : function (Data) {

                    spinner.stop()

                }
            });

        }, error : function (Data) {

            spinner.stop()

            alert("업데이트 실패");
        }
    });

}

function Raw_Update() {


    if(!raw_update_button_result) {
        alert("Raw 버전 정보 및 서버 연결상태를 확인해주세요")
        return
    }

    var target = document.body
    var spinner = new Spinner().spin(target);

    var url = "raw_update";

    $.ajax({
        url: url,
        dataType: "text",
        type: "get",
        success: function (Data) {

            $.ajax({
                url: "script_run",
                data : "process=raw",
                dataType: "text",
                type: "get",
                success: function (Data) {

                    alert("Raw 업데이트 완료")

                    alert("Link 를 재부팅합니다")

                    spinner.stop()

                    $.ajax({
                        url : "/link_reboot",
                        dataType : "text",
                        type: "get",
                        success : function (data) {

                        },error : function (data) {

                        }
                    })

                }, error : function (Data) {

                    alert("Raw 업데이트 실패")

                }, complete : function (Data) {

                    spinner.stop()

                }
            });

        }, error : function (Data) {

            spinner.stop()

            alert("업데이트 실패");
        },
        complete : function() {

        }
    });

}


function isNumber(s) {
    s += ''; // 문자열로 변환
    s = s.replace(/^\s*|\s*$/g, ''); // 좌우 공백 제거
    if (s == '' || isNaN(s)) return false;
    return true;
}