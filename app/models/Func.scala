package models

import java.io._
import java.net.{InetSocketAddress, Socket}
import java.text.SimpleDateFormat
import java.util.Date


/**
  * Created by unomic on 2018. 1. 16..
  */
class Func {




  // 에러메세지 로그(에러코드)
  def Log_error(e : Exception) = {

    var sw = new StringWriter();
    e.printStackTrace(new PrintWriter(sw));
    var exceptionAsStrting = sw.toString();

    try {
      var path = System.getProperty("user.dir") + "/logs/"
      var downld_path = new File(path)
      if(!downld_path.exists()){
        downld_path.mkdirs()
      }

      var log_file = new File(path + "error.log")

      if (log_file.exists() == false) {
        log_file.createNewFile()
      } else {
        if (log_file.length() > 50000000) {
          log_file.delete()
          log_file.createNewFile()
        }

      }

      var buf = new BufferedWriter(new FileWriter(log_file, true))
      buf.append(getTime() + ".  " + exceptionAsStrting)
      buf.newLine()
      buf.close()
    }catch{case e : Exception => e.printStackTrace()}

  }

  // 에러메세지 로그(string 체크 용도 로그)
  def Log_error(log : String) = {
    try {
      var path = System.getProperty("user.dir") + "/logs/"
      var downld_path = new File(path)
      if(!downld_path.exists()){
        downld_path.mkdirs()
      }
      var log_file = new File(path + "error.log")

      if (log_file.exists() == false) {
        log_file.createNewFile()
      } else {
        if (log_file.length() > 50000000) {
          log_file.delete()
          log_file.createNewFile()
        }

      }

      var buf = new BufferedWriter(new FileWriter(log_file, true))
      buf.append(getTime() + ".  " + log)
      buf.newLine()
      buf.close()
    }catch{case e : Exception => e.printStackTrace()}

  }



  // 현재 시간값 return
  def getTime(): String = {
    val time = System.currentTimeMillis
    val dayTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
    val str = dayTime.format(new Date(time))
    return str
  }


  // leng 체크
  def byte_length(str : String) :String = {

    var SendBody : Array[Byte] = (str).getBytes();

    var len = SendBody.length;

    var Len = new Array[Byte](8);
    Len = (Integer.toString(len)).getBytes();

    var SendHead = new Array[Byte](8);

    System.arraycopy(Len, 0, SendHead, 0, Len.length);

    var head_length = new String(SendHead)

    return head_length
  }


  def pingHost(host: String, port: Int): Boolean = {
    var socket = new Socket()

    try {
      socket.connect(new InetSocketAddress(host, port));
      println(socket)
      socket.close()
      return true;
    } catch {
      case e : IOException => {
        socket.close()
        return false
      }
    }
  }


  def runSudoCommand(command : Array[String], pw : String) : String = {

    var output_string = ""

    var runtime = Runtime.getRuntime();
    var process = runtime.exec(command);
    var os = process.getOutputStream();

    val pw_command = pw + "\n"

    //    os.write("gksrkd\n".getBytes());
    os.write(pw_command.getBytes());
    os.flush();
    os.close();
    process.waitFor();

    var output = readFile(process.getInputStream());

    if (output != null && !output.isEmpty()) {
      System.out.println("(command line) : " + output + "\n");
      output_string = output
    }

    var  error = readFile(process.getErrorStream());

    if (error != null && !error.isEmpty()) {
      System.out.println("(command error) : " + error + "\n");
    }

    println("process.exitValue : " + process.exitValue())

    //    return process.exitValue();
    return output_string
  }

  def readFile(inputStream : InputStream) : String = {

    if (inputStream == null) {
      return "";
    }

    var sb = new StringBuilder();

    var bufferedReader : BufferedReader = null

    try {
      bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
      var line = bufferedReader.readLine();
      while (line != null) {
        sb.append(line);
        line = bufferedReader.readLine();
      }
      return sb.toString();
    } finally {
      if (bufferedReader != null) {
        bufferedReader.close();
      }
    }
  }

  // json type 체크( string -> true, false )
  def JsonUtil(str : String): Boolean ={
    try{
      var json_data = new org.json.JSONObject(str)
      return true
    }catch{
      case e : Exception => getTime(); e.printStackTrace(); Log_error(str); Log_error(e);
        return false
    }


  }




}


object Func {

  var func = new Func()

}