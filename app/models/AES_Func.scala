package models

import java.io._
import java.security.MessageDigest
import java.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

/**
  * Created by unomic on 2018. 10. 5..
  */


class AES_Func {

  private val ALGORITHM = "AES"
  private val TRANSFORMATION = "AES"

  private val KEY = "iwanttobealone"
  private val IV = "unomicunomicunom"


  def Encoding(input_file_path : String, output_file_path : String): Boolean = {

    var result = false

    try{

      // encoding

      var s = readFile(input_file_path)
      var res = encrypt(KEY, IV, s)
      var writer = new PrintWriter(output_file_path, "UTF-8")
      writer.print(res)
      writer.close()

      result = true

    } catch{

      case e : Exception => e.printStackTrace();
    }


    return result

  }

  def Decoding(input_file_path : String, output_file_path : String): String ={

    var content = ""

    var s = readFile(input_file_path)
    var res = decrypt(KEY, IV, s)

    var writer = new PrintWriter(output_file_path, "UTF-8")
    writer.print(res)
    writer.close()

    content = res

    return content

  }

  def Decoding_Content(input_file_path : String) : String = {

    var content = ""

    var s = readFile(input_file_path)
    var res = decrypt(KEY, IV, s)

    content = res

    return content

  }


  def test_encrypt_decrypt(): Unit = {

    // encrypt "in.txt" -> "out.txt"
    var s = readFile("in.txt")
    var res = encrypt(KEY, IV, s)
    var writer = new PrintWriter("out.txt", "UTF-8")
    writer.print(res)
    writer.close()
    // decrypt "out.txt" -> "out2.txt"
    s = readFile("out.txt")
    res = decrypt(KEY, IV, s)
    writer = new PrintWriter("out2.txt", "UTF-8")
    writer.print(res)
    writer.close()
  }

  def encrypt(key: String, iv: String, msg: String): String = {
    val bytesOfKey = key.getBytes("UTF-8")
    val md = MessageDigest.getInstance("MD5")
    val keyBytes = md.digest(bytesOfKey)
    val ivBytes = iv.getBytes
    val secretKeySpec = new SecretKeySpec(keyBytes, "AES")
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(ivBytes))
    val resultBytes = cipher.doFinal(msg.getBytes)
    Base64.getMimeEncoder.encodeToString(resultBytes)
  }

  def test_decrypt(): Unit = {
    val s = readFile("in.txt")
    val res = decrypt(KEY, IV, s)
    System.out.println("res:\n" + res)
  }

  def decrypt(key: String, iv: String, encrypted: String): String = {
    val bytesOfKey = key.getBytes("UTF-8")
    val md = MessageDigest.getInstance("MD5")
    val keyBytes = md.digest(bytesOfKey)
    val ivBytes = iv.getBytes
    val encryptedBytes = Base64.getMimeDecoder.decode(encrypted)
    val secretKeySpec = new SecretKeySpec(keyBytes, "AES")
    val cipher = Cipher.getInstance("AES/CBC/PKCS5Padding")
    cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, new IvParameterSpec(ivBytes))
    val resultBytes = cipher.doFinal(encryptedBytes)
    new String(resultBytes)
  }

  def readFile(filename: String): String = {
    val br = new BufferedReader(new FileReader(filename))
    try {
      val sb = new StringBuilder
      var line = br.readLine
      while ( {
        line != null
      }) {
        sb.append(line)
        sb.append(System.lineSeparator)
        line = br.readLine
      }
      val everything = sb.toString
      everything
    } finally br.close()
  }

  def test_read_file(): Unit = {
    val s = readFile("in.txt")
    System.out.println("in.txt:\n" + s)
  }

  def encrypt(key: String, inputFile: File, outputFile: File): Unit = {
    doCrypto(Cipher.ENCRYPT_MODE, key, inputFile, outputFile)
  }

  def decrypt(key: String, inputFile: File, outputFile: File): Unit = {
    doCrypto(Cipher.DECRYPT_MODE, key, inputFile, outputFile)
  }

  def doCrypto(cipherMode: Int, key: String, inputFile: File, outputFile: File) = {
    val secretKey = new SecretKeySpec(key.getBytes, ALGORITHM)
    val cipher = Cipher.getInstance(TRANSFORMATION)
    cipher.init(cipherMode, secretKey)
    val inputStream = new FileInputStream(inputFile)
    val inputBytes = new Array[Byte](inputFile.length.toInt)
    inputStream.read(inputBytes)
    val outputBytes = cipher.doFinal(inputBytes)
    val outputStream = new FileOutputStream(outputFile)
    outputStream.write(outputBytes)
    inputStream.close()
    outputStream.close()
  }

  def test_doCrypto(filename: String): Unit = {
    val key = "Mary has one cat"
    val inputFile = new File("document.txt")
    val encryptedFile = new File("document.encrypted")
    val decryptedFile = new File("document.decrypted")
    encrypt(key, inputFile, encryptedFile)
    decrypt(key, encryptedFile, decryptedFile)
  }

  def test_write_to_file(filename: String): Unit = {
    val writer = new PrintWriter(filename, "UTF-8")
    writer.println("The first line")
    writer.println("The second line")
    writer.close()
  }


}


object AES_Func {

  var afs = new AES_Func()

}