(업데이트 날짜 : 2018.11.09. 16:20)
-------------------

1. 프로젝트 : KISA
2. 언어 : Scala, Play framework
3. 개발 툴 : IntelliJ IDEA Ultimate 2017.1 사용
4. 기능 : Link 내부에 있는 Agent(jane), Raw(nate) 의 버전관리(현재 버전 출력 및 서버 연동해서 업데이트 가능)
5. 준비
	1. Link(Ubuntu) 내부  /root/ 경로에 key 폴더 생성.
		1. key 폴더 안에, FwCertTest.jar 파일 있어야 함
		2. key 폴더 안에, FwCertUtil.jar
		3. key 폴더 안에, util.sh, util2_agent.sh, util3_agent.sh, test_agent.sh 파일 있어야 함
		4. key 폴더 안에, util.sh, util2_raw.sh, util3_raw.sh, test_raw.sh 파일 있어야 함
        5. key 폴더 안에, reboot.sh 파일 있어야함
        6. 모든 스크립트 파일들의 권한은 777 필요
    2. Link 내부 /root/ 경로에 nas_info.json 파일 필요
    3. Link 내부 /root/ 경로에 rest.sh 파일 필요(Link 부팅시 자동으로 펌웨어 실행 시킴)
        1. rest.sh 파일 권한 777 필요
6. 사용 : activator -> 압축파일 생성 후, 압축 해제 -> bin 폴더로 이동 후 명령어 nohup./link_update &  실행(linux)