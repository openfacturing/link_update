name := "link_update"

version := "1.0"

lazy val `link_update` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  jdbc ,
  cache ,
  ws   ,
  "org.apache.httpcomponents" % "httpmime" % "4.3.1",
  "org.json" % "json" % "20160810",
  "commons-io" % "commons-io" % "2.4",
  "com.jcraft" % "jsch" % "0.1.54",
  specs2 % Test )


unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"  